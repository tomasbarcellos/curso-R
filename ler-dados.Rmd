---
title: "Ler dados no R"
output: 
  beamer_presentation: default
  html_document: default
---

```{r setup, include = FALSE}
library(knitr)
options(digits = 3)
RDA <- data.frame(texto = rep(letters[1:25], 4e2),
                  num = seq_len(1e4),
                  stringsAsFactors = FALSE)

write.csv2(x = RDA, file = "teste.csv", row.names = FALSE)
saveRDS(object = RDA, file = "teste.RDS")
save(list = "RDA", file = "teste.RDA")

rm(RDA)
```

O `R` vem como algumas funções nativas para ler dados. Entre elas as mais comuns são:

Função| tipo de arquivo|
------|----------------|
read.table() |".txt", ".csv", ".tsv"|
read.csv()   | ".csv" (sep = ",")   |
read.csv2()  | ".csv" (sep = ";")   |
read.fwf()   | variáveis com tamanhos fixos|
             | muito usado em microdados|
readLines()  | lê linhas como são   |

Vamos testar estas funções?

# Exemplos do R básico

```{r read.csv}
csv <- read.csv2("teste.csv")
str(csv) # Textos se tornaram fatores
csv2 <- read.csv2("teste.csv",
                  stringsAsFactors = FALSE)
str(csv2)
```

# Formatos do R

Função| tipo de arquivo| Quando usar? |
------|----------------|--------------|
saveRDS() | ".RDS" | salvar um objeto  |
readRDS()|".RDS" | ler um objeto |
save() | ".RData" ou ".RDA" | salvar vários objetos |
load() | ".RData" ou ".RDA" | ler vários objetos |

# Formatos do R

```{r RDA_RDS}
RDS <- readRDS("teste.RDS")
str(RDS)
load("teste.RDA") # Não precisa atribuir
str(RDA)
```

# Para que tantos formatos??

Para que eu preciso de tantas formas de ler e escrever dados??

Bem, primeiramente.... fora Temer. Segundo, você geralmente não controla o formato do arquivo que conterá os dados que você usará.  Por isso é importante conhecer variadas formas ler os mais variados formatos. 

# Lendo arquivos do Excel

O **R** "puro" - isto é, assim que você abre ele - não lê arquivos com as extensões clássicas do excel ".xls" e ".xlsx". Contudo, tem muitos pacotes que permitem ler e escrever arquivos com estes formatos.

| Pacote | Bônus| Ônus |
|-----------|-------------|------------|
| `readxl` | muito rápido | não exporta arquivos |
| `xlsx` | exporta arquivos | trava frequentemente |
|        |                  | (acesso por Java) |
| `openxlsx` | rápido, le e |      |
|            | exporta arquivos | mais lento que `readxl` |

Mesmo assim, não recomendo usar ".xlsx" por dois motivos: i) Há um limite de 1 mihão de linhas e ii) até com os bons pacotes, demora a gerar o arquivo.

# Exportando dados

Além das funções de leitura `read.*`, Há também funções para escrever arquivos: `write.*`. Os sufixos se repetem nas duas _famílias_ de funções.

Função| tipo de arquivo|
------|----------------|
write.table() |".txt", ".csv", ".tsv"|
write.csv()   | ".csv" (sep = ",")   |
write.csv2()  | ".csv" (sep = ";")   |
writeLines()  | escreve linhas como são |
openxlsx::write.xlsx() | '.xlsx' |


# Comparando ".xslx" e ".csv"

Até as melhores opções para escrever um ".xlsx" são mais lentas que o **R** básico.

```{r comparacao1}
library(microbenchmark)
library(openxlsx)

microbenchmark(times = 30,
  xlsx = write.xlsx(RDA, "teste.xlsx"),
  csv = write.csv2(RDA, "teste2.csv"))
```

# Comparando a performance
  
```{r comparacao2}

microbenchmark(times = 30, unit = "ms",
  csv = read.csv2("teste.csv"),
  RDA = load("teste.RDA"),
  RDS = readRDS("teste.RDS"),
  readxl = readxl::read_excel("teste.xlsx"),
  openxlsx = read.xlsx("teste.xlsx"))
```


# Tamanho dos formatos

Salvamos os mesmo dados em variados formatos. Vejamos a diferença de tamanho entre eles:

```{r}
file.size("teste.csv") / 2^10 # em Kb
file.size("teste.RDS") / 2^10 # em Kb. 6 vezes menor!
file.size("teste.RDA") / 2^10 # em Kb. Igual!
file.size("teste.xlsx") / 2^10 # em Kb
```

# Dados de outros pacotes estatísticos

O pacote `haven` possui funções para ler formatos de outros softwares como STATA, SPSS e SAS


 software | função
--------- |--------
   SAS    | write_sas
   SAS    | read_sas
  Stata   | write_dta
  Stata   | read_dta
  SPSS    | write_sav
  SPSS    |  read_sav


# Dados de bancos de dados

Há inumeros pacotes para ler dados armazenados em bases de dados. Alguns são:

 pacote |  Base
--------|---------
 RMySQL | MySQL
 RDOBC  | SQL Server
 RPostgres | Postgres
 RSQLite | SQLite
 rmongodb | MongoDB
 RCassandra | Cassandra

# Microdados no Brasil

Trabalhar com microdados de grandes pesquisa não é diferente de trabalhar com quaisquer outros dados, exceto por seu tamanho. Vejamos, por exemplo, duas fonte de microdados no Brasil: Ministério do Trabalho e Previdência Social (MTPS) e o IBGE.

ftp://ftp.mtps.gov.br/pdet/microdados/

ftp://ftp.ibge.gov.br/


# Microdados no Brasil

É possível programar o `R` para fazer download destes arquivos com a função `download.file()` e ler os arquivos com as função mais adequadas a suas extensões (os mesmo ler direto da fonte quando nao estao em '.zip").

Mas nós não fomos os primeiros a pensar em criar estas rotinas. Há duas experiências que devem ser mencionadas:

[Analyze survey data for free](http://www.asdfree.com/)

[Pacote Microdados Brasil](https://github.com/lucasmation/microdadosBrasil)


# asdfree.com

O blog possui inúmeros scripts que fazer downlaod e até uma pequena análise de microdados de vários países, incluindo brasil.

# Pacote Microdados Brasil

É um pacote do `R` (ainda em desenvolvimento) que torna muito fácil baixar e ler microdados.

função | faz
------ |----------
download_sourceData() | download de arquivos
read_* | lê arquivos numa data.table
