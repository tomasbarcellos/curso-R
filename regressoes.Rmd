---
output: 
  beamer_presentation: default
  html_document: default
---

# Fazendo regressões no R

O `R` possui funções próprias para realizar regressões. A principal delas é a função `lm()` e sua sintaxe é bastante direta e apenas um argumento é necessário: a fórmula.

```{r sintaxe, eval = FALSE}
lm(var_explicada ~ log(var_que_explica1) + (var_que_explica2 ^ var_que_explica3))
```

# Fórmula
A fórmula, por sua vez pode ser escrita de várias maneiras.

Sintaxe | modelo                        
------------- | -------------
Y ~ A | Y = β0 + β1A
Y ~ -1 + A | Y = β1A
Y ~ A + I(A^2) | Y = β0+ β1A + β2A^2^
Y ~ A + B | Y = β0+ β1A + β2B
Y ~ A:B | Y = β0 + β1AB
Y ~ A\*B | Y = β0 + β1A + β2B + β3AB
Y ~ (A + B + C)^2 | Y = β0+ β1A + β2B + β3C + β4AB + β5AC + β6AC


# Fazendo seu modelo

Para demonstrar o uso da função `lm()` vamos criar uma variável aleatória `var1` e o `erro` do modelo e vamos construir um `y` baseado neles. Após, pediremos ao `R` que defina a relação entre a variável `var1` e `y` e guarde em  `modelo`.

```{r variaveis}
set.seed(1)
var1 <- rnorm(50) # apenas 50 observações
erro <- rnorm(50)

y = 2.3 * var1 + erro
modelo <- lm(y ~ log(var1))
```

# Conhecendo o modelo

Mas o que é um modelo? Uma fórmula? E os parâmetros? Ele também tem erros, medidas de precisão como o R², etc. Qual delas estará guardada em `modelo`? Vamos descobrir!

```{r str_modelo, eval = FALSE}
print(modelo)
summary(modelo) 
str(modelo)
```

# Visualizando o modelo

A visualização do modelo

```{r vis_modelo1}
plot(log(var1), y)
abline(modelo, col = 'red', lwd = 2) 
```

# Visualizando propriedades do modelo

Outro passo importante na análise de modelos é verificar algumas de suas propriedades e os pressupostos são respeitados.

```{r vis_modelo2}
layout(matrix(1:4, 2, 2))
plot(modelo) 
```

# Modelo Ensino Superior

Agora é sua hora de criar modelos para o lucro das instituições de ensino superior privada em função do número de profissionais em determinado nível de ensino. Crie um modelo para cada um dos níveis de ensino.

```{r, eval=FALSE, include=FALSE}
#####################################################
# Adicionar codigo de leitura dos dados das ies2014 #
#####################################################

library(dplyr)
dado <- ies2014 %>% filter(DS_CATEGORIA_IES == "INSTITUIÇÃO PRIVADA COM FINS LUCRATIVOS")
  mutate(QTD_SUPERIOR = (QT_TEC_SUPERIOR_FEM + QT_TEC_SUPERIOR_MASC),
         QTD_ESPEC = (QT_TEC_ESPECIALIZACAO_FEM + QT_TEC_ESPECIALIZACAO_MASC),
         QTD_MESTRES = (QT_TEC_MESTRADO_FEM + QT_TEC_MESTRADO_MASC),
         QTD_DOUTORES = (QT_TEC_DOUTORADO_FEM + QT_TEC_DOUTORADO_MASC))

dado <- dado[, c("VL_RECEITA_PROPRIA", "QTD_SUPERIOR", "QTD_ESPEC",
                  "QTD_MESTRES", "QTD_DOUTORES")]
formulas <- sapply(paste("VL_RECEITA_PROPRIA ~", names(dado)[-1]), as.formula)
formulas[[5]] <- as.formula(paste("VL_RECEITA_PROPRIA ~", paste(names(dado)[-1], collapse = " + ")))

modelos <- lapply(formulas, lm, data = dado)
names(modelos) <- c(names(dado)[-1], "TUDO")
lapply(modelos, summary)
```