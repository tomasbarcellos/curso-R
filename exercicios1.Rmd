# Exercicios - aula 1

# Rode o seguinte comando para criar um data.frame:
    df <- data.frame(x = rnorm(10), y = rnorm(10))
• Verifique os resutados de length(), nrow(), ncol(), dim(), class(), is.data.frame(),str(),
summary() em df. Quantas observações temos na base de dados? Quantas variáveis? Qual a classe do
objeto? Quais as classes das variáveis?
• Crie um vetor z <- rlnorm(10). Adicione este vetor como mais uma variável, chamada z, em df?
Quais as diferentes formas de fazer isso?
• Como adicionar mais uma linha com x=1, y =2, e z=3 ao data.frame?
• Crie uma variável w em df com o resultado de x+y+z.
• Delete a variável x de df (duas formas.
• Delete as últimas cinco linhas de df.

## data.frame é uma lista.
• Rode o comando is.list() em dados. Qual o resultado?
• A função unclass() retira a classe “especial” dos objetos revertendo-o à sua classe mais básica. Rode o
seguinte código abaixo. Note que um data.frame nada mais é do que uma lista com certas características
(por exemplo: atributo row.names; cada coluna com o mesmo número de linhas).

# Exercicios PNAD

1. Quais os tipos de dados compoe a tabela da PNAD?

2. Quais os tipos de entrevista?

3. Quais os cinco maiores alugueis? E os menores?

4. Quantas casas tem mais de 6 moradores?

5. Selecione apenas a coluna tablet usando `$`, `[` e `[[`.

6. Selecione apenas as colunas de classe numérica.

7. Faça o sumário da tabela `domicilios`.

# Funções para criar

1. Função que converte fator para numérico.

2. Função que conta número de NAs

3. Função que seleciona apenas valores entre dois limites. Adicione um argumento para que retorne um vetor lógico que indica os valores que estão no intervalo solicitado. Limite inferior e/ou superior devem ser incluidos?

"Tao grave, ou pior, e a tragedia da habitaçao. Ha em Cuba 200 mil bohios e choças; 400 mil familias do campo e da cidade vivem amontoadas em barracoes, cortiços e poroes sem a as mais elementares condiçoes de higiene e saude; 2,2 milhoes de pessoas da nossa populaçao urbana  pagam alugueis que absorvem de um quinto a um terço de seus rendimentos; e 2,8 milhoes pessoas de nossa populaçao rurual e suburbana carecem de luz eletrica" Fidel Castro, A Historia me absolvera.

E no Brasil atual? Vamos verificar com os dados da PNAD.