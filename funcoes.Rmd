---
output:
  beamer_presentation: default
  html_document: default
---

# Funções

Estamos usando funções no `R` desde que começamos. Mas o que são funções? As funções são comandos que recebem parâmetros de entrada, fazem alguma coisa e retornam algum resultado.

    função(entrada) # que gera uma
    [1] saida

A função pode ser divida em três partes: nome, argumentos e corpo. Um das grande vantagens do `R` enquanto linguagem de programação é que podemos criar nossas prórpias funções.

```{r funcao_ideia, eval = FALSE}
nome <- function(argumento1, argumento2, ...) {
  # corpo da função
  
  # aqui algo é feito com argumento1 e argumento2
  
}

[1] "Algum resultado"
```

# Sua primeira função

Vamos criar uma função que tem como **entrada** um vetor, que **processa** a contagem dos NAs do vetor e retorna este número (**saída**).

```{r}
conta_na <- function(x) {
  contagem <- sum(is.na(x))
  return(contagem)
}

conta_na(c(NA, 3, NA, 2, 4, 5, NA))

conta_na(1:6)
```

# Por que escrever uma função?

Evita copiar e colar código, o que diminui chance de erro. Além disso, quando você descobrir que há um erro em seu código (e você vai!), só uma alteração será necessária, aquela que define a função.

Você ainda pode usar um função em outra situação. Digamos que você tenha feito esta função `conta_na()` para fazer uma análise da PNAD. Quando você for analisar os dados da RAIS, não vai precisar escrever outra vez o mesmo código.

# Quando escrever a função?

Há uma idéia de que você deve escrever uma função quando executa uma tarefa pela terceira vez. Se você está precisando repetir alguma rotina e está em dúvida se deve escrever um função, provavelmente é porque deve escrevê-la.

# Argumentos padrão

As funções podem ter quantos argumentos você achar necessário. Mas você pode não querer usar todos argumentos em cada vez que chamar a função. Para isso existem os argumentos padrão: se há um padrão para determinado argumento e o usuário determina um valor para este argumento, o padrão é utilizado. Lembra das funções `read.table()` e `read.csv2()`? A diferença entre elas está nos argumentos padrão.

# Argumentos padrão

```{r arg_pad}
conta_na <- function(x, num = TRUE) {
  contagem <- sum(is.na(x))
  if (num == TRUE){
    return(contagem)
  } else {
    return(paste("Há", contagem, "NAs"))
    }
}

vetor <- c(1, 3, NA, 7, NA, 15)
conta_na(num = FALSE, vetor)
elevado(vetor) # ambas funcionam
```

# Um pouco mais sobre argumntos

O `R` tem duas formas de relacionar os argumento que você fornece ao chamar um função com os argumentos formais dela. A primeira é por nome: se os argumentos oferecidos forem nomeados ele busca o nome dos argumentos formais e dá o valor oferecido a eles. A segunda forma é por posição. Reparou que pudemos chamar o argumento `num` antes do `vetor`?

# Argumento especial: "..."

Há um argumento especial que você pode usar nas suas funções: `...`; este argumento é uma lista com todos argumento que você oferece e que não estão especificados na função. Não vamos trabalhar ele aqui, mas você irá se deparar com ele constantemente. Ele geralmente é usado para passar argumentos para uma função que é chamada pela função principal que você está usando.

# Recurssão

Uma função é recursiva quando ela chama a si própria até que ela não chame mais. Funções recursivas são muito mais eficientes Exemplo: função que conta de um número até zero.

```{r recursiva}
ate_0 <- function(x) {
  if (x >= 0) {
    print(x)
    ate_0(x-1) # recursão
  }
}
```

Não se esqueça de especificar um caso base (onde a função para de se chamar). Caso contrário, ocorrerá um loop infinito (que pode ser cancelado com a tacla <kbd> ESQ </kbd> ).

# Operadores binários

Já parou para pensar como o `+` ou o `\` funcionam no R? Sim! Eles também são funções. Isso quer dizer que você pode criar seus próprios operadores se quiser.

```{r op_bin}
`%+%` <- function(x, y) {
  paste(x, y)
}
35 %+% "palavra"
```

# Escopo

Lembra que ao devirmos a função `conta_na` nós definimos uma variável chamada `contagem` no interior dela? O que aconeceria se pedissemos seu valor no console? 

```{r escopo, erro = TRUE}
contagem
```

O `R` não encontra ela porque ela existe apenas no escopo da função. Quando o `R` não encontra ele no ambiente (enviroment) que pedimos, ele procura ambiente pai e depois no pai do pai e assim por diante, até atingir um o ambiente base, que é vazio. Neste caso ele retorna um erro.

# Escopo

Mais e se tivermos uma variável chamada `x` e não oferecermos nenhum argumento a `conta_na()`?

```{r escopo2}
x <- c(1, 2, NA, NA, 8, NA)
conta_na()
```

Neste caso, o `R` não escontra o objeto `x` no escopo da função e busca no seu ambiente pai: o `.GlobalEnviroment`, e a  
função ocorre sem problemas. Mas cuidado: isso pode não ser o que você deseja.
# Métodos

Já observou que dependendo do objeto o `R` decide imprimi-lo diferente, ou mostrar o sumário diferente? Ainda não? Então tente `print(mtcars)` e `print(lm(hp ~ wt, mtcars))`. O `R` faz isso baseado na classe do objeto que passamos a ele. A função `methods()` mostra os métodos de uma função. Veja, por exemplo, `methods(print)`.

O que acontece se usamos a `conta_na()` em um data frame? Ele retorna o número todos de NA em todo o data frame. Mas isso não é tão útil. Queremos que, quando o argumento da função for um data frame, ele diga quantos NAs cada variável tem. Como podemos fazer isso?

# Criando um método

```{r}
conta_na <- function(x) {
  UseMethod("conta_na", x)
}

conta_na.data.frame <- function(df) {
  sapply(df, conta_na)
}

conta_na.default <- function(x) {
  sum(is.na(x))
}
```
