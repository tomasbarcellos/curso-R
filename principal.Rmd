---
output: 
  html_document: 
    self_contained: false
---

```{r, include=FALSE}
tutorial::go_interactive()
```


## Plano de aulas 

O curso ocorrerá entre 13 e 15 de dezembro e está estruturado assim:

* Dia 1, parte 1 - Ler dados no R
* Dia 1, parte 2 - Manupulando data frames
* Dia 2, parte 1 - Manipulando dados com dplyr
* Dia 2, parte 2 - Regressões
* Dia 3, parte 1 - Visualização de dados com ggplot2
* Dia 3, parte 2 - Exercícios propostos pela turma

Para fazer download dos dados utilizados na aula acesse [esta pasta](https://www.dropbox.com/sh/lmpv47nckwsq4hq/AADYZ-vg2KqfW468RsLYvs9Fa?dl=0) do dropbox.

Todas as aulas ainda podem ser alteradas. Exercícios seão realizados em sala para fixar as filosofias dos pacotes e seus comandos.


### Aquecimento

Que tal já utilizar um pouco dos seus conhecimentos? Tente realizar o exercício abaixo. Caso não consiga realizar as duas primeiras tarefas, revisite o [curso introdutório](https://www.datacamp.com/courses/2315). A terceira é um desafio.


```{r ex = "primeiro", type="sample-code"}
# Já sabe criar uma variável? Atribuia 20 à x


# Verifique a estrutura da tabela iris com str()


# Calcule a médias do cumprimento das pétalas por espécie

```

```{r ex = "primeiro", type="solution"}
# Já sabe criar uma variável? Atribuia 20 à x
x <- 20

# Verifique a estrutura da tabela iris com str()
str(iris)

# Calcule a médias do cumprimento das pétalas por espécie
aggregate(Petal.Length ~ Species, data = iris, mean)
```

```{r ex = "primeiro", type="sct"}
test_object("x")
test_student_typed(c("tapply(iris$Petal.Length, iris$Species, mean)",
                     "aggregate(Petal.Length ~ Species, data = iris, mean)"),
                   not_typed_msg = "Nao conseguiu achar uma solucao? Fique tranquilo, você sabera ao final do curso. Ate la você pode clicar em `solution` e ver a solucao proposta (nao eh a unica).")
success_msg("Mandou bem!!")
```