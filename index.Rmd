---
title: "Curso de R"
output: 
  html_document: 
    self_contained: false
---

```{r pagina, echo = FALSE, warning = FALSE}
library(shiny)
print(
  navbarPage('', theme = 'css/hyde-x.css',
             tabPanel('Principal',
                      HTML(paste(readLines('principal.html',
                                           encoding = "UTF-8"),
                                 collapse = "\n"))),
             tabPanel('Importação de dados',
                      HTML(paste(readLines('ler-dados.html',
                                           encoding = "UTF-8"),
                                 collapse = "\n"))),
             tabPanel('Manipulação de dados',
                      HTML(paste(readLines('manipulando_dados.html',
                                           encoding = "UTF-8"),
                                 collapse = "\n"))),
             tabPanel('dplyr',
                      HTML(paste(readLines('dplyr.html',
                                           encoding = "UTF-8"),
                                 collapse = "\n"))),
             tabPanel('Regressoes',
                      HTML(paste(readLines('regressoes.html',
                                           encoding = "UTF-8"),
                                 collapse = "\n"))),
             tabPanel('Programando',
                      navbarPage('',
                      tabPanel('Funções',
                      HTML(paste(readLines('funcoes.html',
                                           encoding = "UTF-8"),
                                 collapse = "\n"))),
                      tabPanel('Loops',
                      HTML(paste(readLines('for.html',
                                           encoding = "UTF-8"),
                                 collapse = "\n"))),
                      tabPanel('For',
                               HTML(paste(readLines('for.html',
                                                    encoding = "UTF-8"),
                                          collapse = "\n"))
                      ))
             ),
             tabPanel('Visualização de dados',
                      HTML(paste(readLines('ggplot2.html',
                                           encoding = "UTF-8"),
                                 collapse = "\n"))),
             selected = "Principal"
  ), quote = FALSE)
```


<footer align = "center" >
Clique [aqui](https://github.com/R-UFSC/r-ufsc.github.io) para acessar o repositório no github
</footer>