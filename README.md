# Curso de R - UFSC 2016

## Introdução a anáslise de dados no R

### Estrutura do curso

1. R e RStudio: O que são e qual a cara?

2. Como buscar ajuda:

    2.1. Função help() ou `?`

    2.2. StackOverflow em Português

3. O R como calculadora: operadores básicos e sintaxe

4. Variáveis: tipos e características

5. Operando com vetores: especificidades, coerção e reciclagem

6. Listas e data.frames

7. Entrada e saída de dados  

    7.1. Formatos mais uteis e utilizados:  
    
        7.1.1. Formatos do R:

    a) .RDA e 

    b) .RDS 

        7.1.2. Outro formatos:

    a) .csv; 

    b) .txt; 

    c) .xlsx;

    d) bases de dados.

8. Manipulando dados com o R básico

    8.1. funções subset, with, Filter, `[` e `[[`

9. Manipulando dados com o dplyr

    9.1. Uma gramática para manipulação de dados

    9.2. Os verbos do dplyr:

    * group_by, filter, select, summarise, mutate, arrange

10. Trabalhando MicroDados do Brasil

    10.1. O pacote MicrodadosBrasil: caso CensoEducação Suprior.

    10.2. Os scripts do J.Damico e Djalma: caso PNAD.

11. Introdução a visualização de dados: ggplot2
